## Certbot Renewal Script

Script en Bash que verifica si los dominios en los certificados gestionados por una instancia de Certbot están en condiciones de renovarse. Para esto se usa el comando `certbot renew --dry-run`.

Si surge algún problema con la renovación, el script enviará una notificación por correo electrónico a un administrador.

Si no hay ningún problema se procede con la renovación a través de `certbot renew -q`.

### Funcionamiento

El script `certbot_renew.sh` realiza las siguientes acciones:

1. Ejecuta una simulación de renovación de certificados usando `certbot renew --dry-run`.
2. Si la simulación falla, registra la salida en el archivo de registro especificado y envía un correo electrónico de notificación al administrador.
3. Si la simulación tiene éxito, procede a la renovación real de los certificados utilizando `certbot renew`.

### Instalación

1. Cloná el repositorio:

   ```bash
   git clone https://gitlab.com/2230/certbot-safe-renewer
   ```

2. Editá el archivo `certbot_renew.sh` y configurá las variables `FROM` y `DESTINATION` con el mail de origen y el destinatario de la notificación en caso de error:

   ```bash
   nano certbot_renew.sh 
   ```

3. Mové el archivo `certbot_renew.sh` a la ruta `/usr/local/bin/`:

   ```bash
   sudo mv certbot_renew.sh /usr/local/bin/
   ```

4. Edita el archivo de servicio `certbot.service`:

   ```bash
   sudo systemctl edit certbot.service
   ```

5. Modificá `ExecStart` para que se ejecute `/usr/local/bin/cerbot_renew.sh`. El archivo debería quedar mas o menos así:

   ```plaintext
   [Unit]
   Description=Certbot
   Documentation=file:///usr/share/doc/python-certbot-doc/html/index.html
   Documentation=https://certbot.eff.org/docs
   
   [Service]
   Type=oneshot
   ExecStart=/usr/local/bin/cerbot_renew.sh
   PrivateTmp=true
   ```

   ```bash
   sudo systemctl daemon-reload
   ```

### Uso

Este script ha sido pensado para ser utilizado en sistemas Linux que tengan Certbot instalado, cuenten con certificados generados y utilicen el sistema systemctl para ejecutar la renovación de automática 

Asegurate de tener la configuración necesaria para que `certbot renew` sepa cómo renovar los certificados que administra. Generalmente esta configuración se encuentra en `/etc/letsencrypt/renewal/`


Una vez completados los pasos de instalación, el script se ejecutará automáticamente según la configuración del servicio. Si se detecta algún problema con la renovación de los certificados, recibirás una notificación por correo electrónico en la dirección especificada.

