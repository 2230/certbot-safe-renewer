#!/bin/bash

LOG_FILE="/var/log/certbot_check.log"
FROM="replaceme"
DESTINATION="root"
HOSTNAME=$(hostname)

certbot_output=$(certbot renew --dry-run 2>&1)

if [ $? -ne 0 ]; then
        echo "$certbot_output" >> "$LOG_FILE"
        echo "$certbot_output" | mail -s "Certbot Error $HOSTNAME" -a "From: $FROM" $DESTINATION
        exit 1
fi

certbot_renew_output=$(certbot -q renew 2>&1)

if [ $? -ne 0 ]; then
        echo "$certbot_renew_output" | mail -s "Certbot Error $HOSTNAME" -a "From: $FROM" $DESTINATION
        exit 1
fi
